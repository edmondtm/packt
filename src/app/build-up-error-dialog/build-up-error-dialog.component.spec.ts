import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildUpErrorDialogComponent } from './build-up-error-dialog.component';

describe('BuildUpErrorDialogComponent', () => {
  let component: BuildUpErrorDialogComponent;
  let fixture: ComponentFixture<BuildUpErrorDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildUpErrorDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildUpErrorDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
