import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';

import { FlexLayoutModule } from '@angular/flex-layout';
import { HomepageComponent } from './homepage/homepage.component';
import { MeasureComponent } from './measure/measure.component';
import { ChecklistComponent } from './checklist/checklist.component';
import { BuildupComponent } from './buildup/buildup.component';
import { SummaryComponent } from './summary/summary.component';
import { HttpClientModule } from '@angular/common/http';
import { BackendService } from './backend.service';
import { WebcamModule } from 'ngx-webcam';
import { MockService } from './mock.service';
import { CountdownModule } from 'ngx-countdown';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { BuildUpErrorDialogComponent } from './build-up-error-dialog/build-up-error-dialog.component';
import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';

@NgModule({
  entryComponents: [BuildUpErrorDialogComponent],
  declarations: [
    AppComponent,
    HomepageComponent,
    MeasureComponent,
    ChecklistComponent,
    BuildupComponent,
    SummaryComponent,
    BuildUpErrorDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    HttpClientModule,
    WebcamModule,
    CountdownModule,
    ZXingScannerModule
  ],
  providers: [BackendService, MockService, { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: false } }],
  bootstrap: [AppComponent]
})
export class AppModule { }
