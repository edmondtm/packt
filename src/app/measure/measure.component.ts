import { Component, OnInit } from '@angular/core';
import { Awb } from '../model/awb';
import { Observable, Subject } from 'rxjs';
import { WebcamImage, WebcamUtil } from 'ngx-webcam';
import { MockService } from '../mock.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-measure',
  templateUrl: './measure.component.html',
  styleUrls: ['./measure.component.css']
})
export class MeasureComponent implements OnInit {

  constructor(private mockService: MockService, private router: Router) { }
  isLoading: false;
  awbs: any[];
  public webcamImage: WebcamImage = null;
  private trigger: Subject<void> = new Subject<void>();
  public multipleWebcamsAvailable = false;
  displayedColumns: string[] = ['natureAndQuatityOfGoods'];
  images: any[] = [];
  selectedAwb: Awb;
  data: any;
  isCameraActive: boolean = true;
  imageResult: any;
  currentScannedPieces = 0;
  scanResults: any[] = [];

  ngOnInit() {
    WebcamUtil.getAvailableVideoInputs()
      .then((mediaDevices: MediaDeviceInfo[]) => {
        this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
      });

    this.getAWB();
  }

  getAWB(): void {
    this.awbs = this.mockService.getAirwayBills();
  }

  public handleImage(webcamImage: WebcamImage): void {
    console.info('received webcam image', webcamImage);
    this.images.push(webcamImage);
    console.log(this.images.length);
    if (this.images.length === 2) {
      // Object.assign(this.data, {top: this.images[0].imageAsBase64});
      this.data = {};
      this.data.top = this.images[0].imageAsDataUrl;
      this.data.side = this.images[1].imageAsDataUrl;
      this.mockService.postImages(this.data)
        .subscribe(s => {
          console.log(s)
          this.currentScannedPieces++;
          this.imageResult = s;
          this.scanResults.push(this.imageResult);
          sessionStorage.setItem('scanResults', JSON.stringify(this.scanResults));
        });
    }
  }

  triggerSnapshot(): void {
    this.isCameraActive = false;
    this.playAudio();
    this.trigger.next();
    if (this.images.length !== 2) {
      this.isCameraActive = true;
    }
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }

  onSelectAwb(e) {
    this.selectedAwb = this.awbs.filter(awb => awb.id === e.value)[0];
    console.log(this.selectedAwb);
  }

  onClickAwb(awb: any) {
    this.selectedAwb = awb;
    console.log(this.selectedAwb);
  }

  scanNext() {
    this.images = [];
    this.isCameraActive = true;
  }
  playAudio() {
    const audio = new Audio();
    audio.src = "../../assets/beep.wav";
    audio.load();
    audio.play();
  }
}
