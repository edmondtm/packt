import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildupComponent } from './buildup.component';

describe('BuildupComponent', () => {
  let component: BuildupComponent;
  let fixture: ComponentFixture<BuildupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
