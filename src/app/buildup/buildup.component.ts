import { Component, OnInit } from '@angular/core';
import { BuildUpErrorDialogComponent } from '../build-up-error-dialog/build-up-error-dialog.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-buildup',
  templateUrl: './buildup.component.html',
  styleUrls: ['./buildup.component.css']
})
export class BuildupComponent implements OnInit {
  displayedColumns: string[] = ['awb', 'pieces'];
  totalWeight: number = 0;
  totalPieces: number = 0;
  constructor(public dialog: MatDialog) {
  }

  scannedResults = [
  ];

  ngOnInit() {

  }
  scanSuccess(e: any) {
    const awb = e.replace('\'', '').replace('\'', '');
    if (awb === 'AWB1234') {
      this.openDialog();
      return;
    }
    const index = this.scannedResults.findIndex(f => f.awb === awb);

    if (index > -1) {
      this.scannedResults[index].pieces = this.scannedResults[index].pieces + 1;
    } else {
      this.scannedResults.push({ awb, pieces: 1 });
    }
    this.totalPieces = this.totalPieces + 1;
    this.totalWeight = this.totalWeight + Math.floor(Math.random() * 500) + 200;
    this.playAudio();
    console.log(this.scannedResults)
  }

  playAudio() {
    const audio = new Audio();
    audio.src = "../../assets/beep.wav";
    audio.load();
    audio.play();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(BuildUpErrorDialogComponent, {
      width: '250px',
      data: { message: 'AWB1234 destination airport is not PUS' }
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }

}
