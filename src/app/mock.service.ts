import { Injectable } from '@angular/core';
import { Awb } from './model/awb';
import { HttpClient } from '@angular/common/http';
import { tap, combineAll, map } from 'rxjs/operators';
import { combineLatest } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MockService {

  constructor(private http: HttpClient) { }
  targetDate = new Date();
  MockAWB: any[] = [
    {
      id: 'AWB843-0111 2250',
      destination: 'JWT',
      itemCount: 1,
      items: [
        {
          noOfPieces: 1,
          weight: 224.0,
          natureAndQuantityOfGoods: '1P/T (50C/T) OF MATT OYSTER-150g & ETC'
        }
      ],
      requestShipTime: this.targetDate.setHours(this.targetDate.getHours() + 1)
    },
    {
      id: 'AWB843-0222 2250',
      destination: 'JHB',
      itemCount: 2,
      items: [
        {
          noOfPieces: 1,
          weight: 224.0,
          natureAndQuantityOfGoods: '1P/T (50C/T) OF MATT OYSTER-150g & ETC'
        },
        {
          noOfPieces: 1,
          weight: 224.0,
          natureAndQuantityOfGoods: '1P/T (50C/T) OF MATT OYSTER-150g & ETC'
        }
      ],
      requestShipTime: this.targetDate.setHours(this.targetDate.getHours() + 2)
    },
    {
      id: 'AWB843-0333 2250',
      destination: 'JHB',
      itemCount: 3,
      items: [
        {
          noOfPieces: 1,
          weight: 224.0,
          natureAndQuantityOfGoods: '1P/T (50C/T) OF MATT OYSTER-150g & ETC'
        }
      ],
      requestShipTime: this.targetDate.setHours(this.targetDate.getHours() + 3)
    },
    {
      id: 'AWB843-0444 2250',
      destination: 'JHB',
      itemCount: 4,
      items: [
        {
          noOfPieces: 1,
          weight: 224.0,
          natureAndQuantityOfGoods: '1P/T (50C/T) OF MATT OYSTER-150g & ETC'
        },
        {
          noOfPieces: 1,
          weight: 224.0,
          natureAndQuantityOfGoods: '1P/T (50C/T) OF MATT OYSTER-150g & ETC'
        },
        {
          noOfPieces: 1,
          weight: 224.0,
          natureAndQuantityOfGoods: '1P/T (50C/T) OF MATT OYSTER-150g & ETC'
        }
      ],
      requestShipTime: this.targetDate.setHours(this.targetDate.getHours() + 4)
    },
    {
      id: 'AWB843-0555 2250',
      destination: 'JHB',
      itemCount: 5,
      items: [
        {
          noOfPieces: 1,
          weight: 224.0,
          natureAndQuantityOfGoods: '1P/T (50C/T) OF MATT OYSTER-150g & ETC'
        },
        {
          noOfPieces: 1,
          weight: 224.0,
          natureAndQuantityOfGoods: '1P/T (50C/T) OF MATT OYSTER-150g & ETC'
        },
        {
          noOfPieces: 1,
          weight: 224.0,
          natureAndQuantityOfGoods: '1P/T (50C/T) OF MATT OYSTER-150g & ETC'
        },
        {
          noOfPieces: 1,
          weight: 224.0,
          natureAndQuantityOfGoods: '1P/T (50C/T) OF MATT OYSTER-150g & ETC'
        },
        {
          noOfPieces: 1,
          weight: 224.0,
          natureAndQuantityOfGoods: '1P/T (50C/T) OF MATT OYSTER-150g & ETC'
        }
      ],
      requestShipTime: this.targetDate.setHours(this.targetDate.getHours() + 5)
    }
  ];

  getAirwayBills(): Awb[] {
    console.log(this.MockAWB);
    return this.MockAWB;
  }

  postImages(data) {
    console.log('post image');
    const url = 'https://54.251.135.204:80/detect_dim_b64';
    const top = new FormData();
    top.append('file', data.top);
    const side = new FormData();
    side.append('file', data.side);
    console.log('data', data);

    const top$ = this.http.post(url, top);
    const side$ = this.http.post(url, side);

    return combineLatest(top$, side$)
      .pipe(
        map((images: any) => {
          const width = images[0].width;
          const length = images[0].length;
          let height = 0;
          if (Math.abs(images[1].width - width) > Math.abs(images[1].length - width)) {
            height = images[1].length
          } else {
            height = images[1].width
          }
          const weight = images[1].weight;
          const volumeMetric = (width * length * height) / 6000;
          return {
            width,
            length,
            height,
            weight,
            volumeMetric,
            finalWeight: Math.max(weight, volumeMetric),
            topImageUrl: images[0].url,
            sideImageUrl: images[1].url,
          }
        })
      )
  }
}
