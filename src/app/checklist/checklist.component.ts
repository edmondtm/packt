import { Component, OnInit } from '@angular/core';

export interface ShipmentMeasurement {
  qty: number;
  length: number;
  width: number;
  height: number;
  volumetric: number;
}

const ELEMENT_DATA: ShipmentMeasurement[] = [
  {
    qty: 1,
    length: 150,
    width: 150,
    height: 400,
    volumetric: 123456789
  },
  {
    qty: 1,
    length: 250,
    width: 150,
    height: 400,
    volumetric: 123456789
  },
  {
    qty: 1,
    length: 150,
    width: 250,
    height: 400,
    volumetric: 123456789
  },
];



@Component({
  selector: 'app-checklist',
  templateUrl: './checklist.component.html',
  styleUrls: ['./checklist.component.css']
})
export class ChecklistComponent implements OnInit {
  scanResults: any[];
  constructor() { }

  displayedColumns: string[] = ['qty', 'length', 'width', 'height', 'volumetric'];
  dataSource = ELEMENT_DATA;

  questions: string[] = [
    'Accompanied by original AWB',
    'Piece and Weight correspond with AWB',
    'Have all the relevent authority been cleared ?',
    `Special cargo such as HUM, DGR, AVI are packed in accordance to
    IATA requirements and attached with supporting documents`,
    'Adequate packing ?',
    'Marking, label, pieces and weight correspond with acceptance sheet ?',
    'Packing can be loaded in the aircraft',
    'Dimension check (for volumetric weight)',
    'Handle all shipment with care according to the handling labels and handling instructions'
  ];

  ngOnInit() {
    if (sessionStorage.getItem('scanResults')) {
      this.scanResults = JSON.parse(sessionStorage.getItem('scanResults'));
    }

  }

}
