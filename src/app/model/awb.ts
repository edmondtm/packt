export interface Awb {
    id: string;
    city: string;
    itemCount: number;
    items?: Item[];
}

export interface Item {
    width: number;
    height: number;
    length: number;
    weight: number;
    topview?: string;
    sideview?: string;
}
