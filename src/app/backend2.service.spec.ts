import { TestBed } from '@angular/core/testing';

import { Backend2Service } from './backend2.service';

describe('Backend2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Backend2Service = TestBed.get(Backend2Service);
    expect(service).toBeTruthy();
  });
});
