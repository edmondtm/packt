import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { MeasureComponent } from './measure/measure.component';
import { BuildupComponent } from './buildup/buildup.component';
import { ChecklistComponent } from './checklist/checklist.component';
import { SummaryComponent } from './summary/summary.component';

const routes: Routes = [
  {path: 'homepage', component: HomepageComponent},
  {path: 'measure', component: MeasureComponent},
  {path: 'buildup', component: BuildupComponent},
  {path: 'checklist', component: ChecklistComponent},
  {path: 'summary', component: SummaryComponent},
  {path: '', pathMatch: 'full', redirectTo: '/homepage'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
